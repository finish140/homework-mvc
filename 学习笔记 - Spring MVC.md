# 学习笔记 - Spring MVC

## 1. MVC模式

### 1.1 MVC模式概述

**MVC指的是一种B/S架构应用的代码结构组织方式，其中MVC分别是：Model、View、Controller。**

- Controller（控制器）

  负责接收请求、转发请求的，例如Servlet。

- View（视图）

  负责数据展示、用户交互，如html、jsp。

- Model（模型）

  包括数据模式和业务模型，数据模型指的就是封装数据的POJO，业务模型指的是封装业务逻辑代码的模型

**在B/S架构应用的代码基础结构上，一般都使用经典三层架构，分别是：表现层、业务层、持久层。而MVC是表现层中的代码结构。**

- 表现层

  由View和Controller组成，View负责视图的渲染，而Controller负责接收请求、转发请求等。它们共同完成与用户的交互，它们之间通过Model进行通讯，这也是MVC的体现。

- 业务层

  由Controller调用业务层，业务层完成对数据业务逻辑操作。

- 持久层

  由Service发起调用，负责对持久化数据的读写操作。



## 2 Spring MVC

SpringMVC有个老大：**DispatcherServlet**，它是一个Servlet，也是Spring MVC中唯一一个Servlet，它负责接收所有请求（可配置）。它本身并不负责对请求的处理，而是由它发起调用，使用各个组件，完成对请求处理。



### 2.1 Spring MVC九大组件

1. **HandlerMapping**

   **处理器映射器**。Spring MVC的核心组件之一，它负责维护Handler与请求之间的映射关系，Handler的表现形式可以是类，也可以是方法，标注了@RequestMapping的每个方法都可以看作是一个Handler。当请求到达后，DispatcherServlet会调用HandlerMapping，返回的是一个HandlerExecutionChain（处理器执行链），它包括了请求相对应的Handler和HandlerInterceptor。

2. **HandlerAdapter**

   **处理器适配器**。Spring MVC的核心组件之一，在Spring MVC中，Handler可以有多种形式，不同的Handler形式需要有不同的处理方式，例如参数的绑定、反射的执行等。HandlerAdapter就相当于是DispatcherServlet的一个中介，不同的HandlerAdapter能够对不同形式的Handler进行适配，从而能够成功执行Handler。

3. **HandlerExceptionResolver**

   **处理器异常解析器**。它负责解析Handler产生的异常，并将异常信息设置到ModelAndView中，之后交给渲染方法进行渲染，渲染方法会将ModelAndView渲染成页面。

4. **ViewResolver**

   **视图解析器**。它负责将视图名和Locale解析为View。Controller层返回一个String类型的viewName，而ViewResolver会把这个viewName解析成View，View是用来渲染页面的，也就是说，它会将程序返回的参数和数据填入模版中，生成html文件。ViewResolver在这个过程中主要完成两件事情，一是找到渲染所用的模版，二是找到渲染所用的技术，如JSP等。Spring MVC会自动配置一个InternalResourceViewResolver，是针对JSP类型视图的。

5. **RequestToViewNameTranslator**

   **请求视图名转换器**。当Handler处理完成后，没有设置View，也没有设置viewName，就要通过这个组件从请求中获取一个适当的viewName。

6. **LocaleResolver**

   **本地化解析器**。负责国际化操作的组件，用于从请求出解析出Locale，比如中国Locale是zh-CN，用来表示一个区域。这个组件也是i18n的基础。

7. **ThemeResolver**

   **主题解析器**。主题指的是样式、图片、显示效果等的集合。它用来切换视图的不同主题。

8. **MultipartResolver**

   **多媒体解析器**。主要用于文件上传请求，将普通的请求包装成MultipartHttpServletRequest来实现。MultipartResolver的作用就是封装普通的请求，使其拥有文件上传的功能。

9. **FlashMapManager**

   FlashMapManager就是用来管理FlashMap的。FlashMap用于重定向时的参数传递，在重定向时，如果不想把参数写在URL中，就需要通过FlashMap来进行参数的传递。在重定向之前向要传递的数据写入请求（可以通过ServletRequestAttribute.getRequest()方法获得）的属性OUTPUT_FLASH_MAP_ATTRUBUTE中，这样在重定向之后的Handler中Spring会向Model设置参数，页面就可以直接从Model中获取数据。



### 2.2 Spring MVC请求处理流程

![Spring MVC请求处理流程](http://gitee.com/finish140/homework-mvc/raw/master/springMVC_process.png)

