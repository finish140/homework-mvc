作业分为两个模块，framework-mvc是手写MVC框架的部分，homework-sss是sss整合的部分。

两个模块的作业分别都有录制演示视频，在项目的根目录中：
- framework-mvc.mov：手写mvc作业的演示视频
- homework-sss.mov：整合SSS作业的演示视频

手写mvc的用户权限表：

| 用户     | Handler           |
| -------- | ----------------- |
| 绿绿     | /xiaohong         |
| 大绿子   | /renqi            |
| 隔壁老王 | /xiaohong, /renqi |



## framework-mvc
### 概述
mvc容器初始化的流程大致上与老师的视频讲解相同，不同的地方在于在建立处理器映射器的时候，进行了对@Security注解的解析，并将对应的用户权限封装到Handler中。

当请求过来的时候，会通过判断request域中`username`的参数值，与Handler中的用户权限进行匹配校验，从而进行不同的响应。

#### Handler
封装了必要的处理器信息，利用Set集合`usernames`存储该Handler的用户权限信息。
``` java
public class Handler {

  /** Handler对应的uri */
  private Pattern uri;

  /** Handler方法 */
  private Method method;

  /** Handler所在的Controller对象 */
  private Object controller;

  /** 参数位置映射，用于进行Handler参数的绑定 */
  private Map<String, Integer> paramIndexMapping;

  /** 表示handler可以被哪些用户访问 */
  private Set<String> usernames;

  public Handler(Pattern uri, Method method, Object controller) {
    this.uri = uri;
    this.method = method;
    this.controller = controller;
    this.paramIndexMapping = new HashMap<>(4);
    this.usernames = new HashSet<>(4);
  }
}

// setter getter...
```



### DispatcherServlet.initHandlerMapping()

这个方法负责读取并封装Handler，在此过程中读取@Security相关的用户名信息，封装到Handler中。

```java
// 如果在Controller的类上标注了@Security，读取其中的用户名信息
Set<String> usernames = new HashSet<>(4);
if (beanClass.isAnnotationPresent(Security.class)) {
  Security security = beanClass.getDeclaredAnnotation(Security.class);
  String[] value = security.value();
  if (value.length > 0) {
    usernames.addAll(Arrays.asList(value));
  }
}


...

  
// 如果handler方法标注了@Security，读取其中的用户名信息，与Controller的@Security中的用户名信息进行合并
if (method.isAnnotationPresent(Security.class)) {
  Security security = method.getDeclaredAnnotation(Security.class);
  String[] value = security.value();
  if (value.length > 0) {
    // 添加所有方法上的用户权限
    handler.getUsernames().addAll(Arrays.asList(value));
  }
}

// 添加所有类上的用户权限
handler.getUsernames().addAll(usernames);
```



### DispatcherServlet.doPost()

doPost()方法，在处理请求过程中验证请求的用户信息。

```java
String username = req.getParameter("username");
boolean hasAuth = false;

for (String s : usernames) {
  if (s.equals(username)) {
    hasAuth = true;
    break;
  }
}

// 没有权限，返回
if (!hasAuth) {
  resp.setCharacterEncoding("UTF-8");
  resp.setStatus(403);
  resp.setHeader("Content-type", "text/html;charset=UTF-8");
  resp.getWriter().write("用户: " + username + " 没有访问此地址的权限");
  return;
}
```



## homework-sss

### 概述

这个项目有没有使用tomcat插件，tomcat7插件跟高版本的Hibernate以及jdk不兼容，所以项目启动时需要依靠外部Tomcat容器。

尝试了以完全脱离xml配置文件，以纯java的方式进行了SSS整合。

主要有3个配置类，都在`org.fin.config`包中：

- **WebApplicationConfiguration**

  配置web容器，对应web.xml

- **SpringConfiguration**

  配置IoC容器，以及数据源、JPA、Hibernate等

  对应applicationContext.xml

- **SpringMvcConfiguration**

  配置Spring MVC，对应spring-mvc.xml



另外，登录功能通过MVC拦截器完成。拦截流程：拦截器配置拦截规则为`/**`，将静态资源，以及登录页面和登录请求的url除外。

登录成功时，会以特定key向session对象放入一个空对象，拦截器向session获取此对象，如果能获取到，代表已登录，否则代表未登录，并进行登录页面的跳转。



### 纯Java方式SSS整合

#### - WebApplicationConfiguration

这个类继承了`AbstractAnnotationConfigDispatcherServletInitializer`，这个父类中，已经将`DispatcherServlet`进行了实例化，我们只需要配置DispatcherServlet的url-pattern、Spring容器以及Spring MVC的配置类即可。

这个类会在Web容器启动时，进行初始化，具体的原理跟`ServletContainerInitializer`相关，这里不进行阐述。

```java
public class WebApplicationConfiguration extends AbstractAnnotationConfigDispatcherServletInitializer {

  /**
   * 配置DispatcherServlet的url-pattern
   */
  @Override
  protected String[] getServletMappings() {
    return new String[]{"/"};
  }

  /**
   * 配置Spring的配置类
   */
  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class[]{SpringConfiguration.class};
  }

  /**
   * 配置Spring MVC的配置类
   */
  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class[]{SpringMvcConfiguration.class};
  }

  /**
   * 配置过滤器
   */
  @Override
  protected Filter[] getServletFilters() {
    CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
    characterEncodingFilter.setEncoding("UTF-8");
    characterEncodingFilter.setForceEncoding(true);
    return new Filter[]{
        new HiddenHttpMethodFilter(),
        characterEncodingFilter
    };
  }
}
```



#### - SpringConfiguration

跟xml配置的形式差不多，只不过将bean标签换成了@Bean的形式。这里配置了数据源、Hibernate相关配置，事务相关配置，以及Service层、DAO层以及POJO的包扫描路径等。

```java
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "org.fin.repository")
@ComponentScan("org.fin.service")
@PropertySource("classpath:jdbc.properties")
@Configuration
public class SpringConfiguration {

  @Value("${jdbc.driverClass}")
  private String driverClass;
  @Value("${jdbc.url}")
  private String url;
  @Value("${jdbc.username}")
  private String username;
  @Value("${jdbc.password}")
  private String password;

  @Bean(initMethod = "init", destroyMethod = "close")
  public DruidDataSource dataSource() {
    DruidDataSource dataSource = new DruidDataSource();
    dataSource.setDriverClassName(driverClass);
    dataSource.setUrl(url);
    dataSource.setUsername(username);
    dataSource.setPassword(password);
    return dataSource;
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory(
      DataSource dataSource,
      JpaVendorAdapter jpaVendorAdapter)
  {
    LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
    // 设置数据源
    factoryBean.setDataSource(dataSource);
    // 设置实体类的包扫描
    factoryBean.setPackagesToScan("org.fin.pojo");
    // 设置JPA具体实现：Hibernate
    factoryBean.setPersistenceProvider(new HibernatePersistenceProvider());
    // 设置JPA方言：Hibernate
    factoryBean.setJpaDialect(new HibernateJpaDialect());
    // 配置具体Provider，Hibernate框架的具体细节s
    factoryBean.setJpaVendorAdapter(jpaVendorAdapter);
    return factoryBean;
  }

  @Bean
  public JpaVendorAdapter jpaVendorAdapter() {
    HibernateJpaVendorAdapter jva = new HibernateJpaVendorAdapter();
    // 是否自动创建表
    jva.setGenerateDdl(false);
    // 是否打印执行的sql
    jva.setShowSql(true);
    // 配置数据库类型
    jva.setDatabase(Database.MYSQL);
    // 配置数据库方言
    jva.setDatabasePlatform("org.hibernate.dialect.MySQLDialect");
    return jva;
  }

  @Bean
  public TransactionManager transactionManager(DataSource dataSource, EntityManagerFactory entityManagerFactory) {
    JpaTransactionManager txm = new JpaTransactionManager();
    txm.setEntityManagerFactory(entityManagerFactory);
    txm.setDataSource(dataSource);
    return txm;
  }

}
```



#### - SpringMvcConfiguration

主要通过注解`@EnableWebMvc`进行默认的配置，实现`WebMvcConfigurer`的不同方法进行自定义配置来完成对Spring MVC的配置。

这里配置了Controller层的包扫描路径，以及静态资源路径映射，以及注册一个登录拦截器。

```java
@EnableWebMvc
@Configuration
@ComponentScan("org.fin.controller")
public class SpringMvcConfiguration implements WebMvcConfigurer {

  /**
   * 启用Web容器中默认的Servlet
   */
  @Override
  public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
    configurer.enable();
  }

  /**
   * 配置静态资源映射
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/statics/**").addResourceLocations("/statics/");
  }

  /**
   * 配置拦截器
   */
  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(new LoginInterceptor())
        .addPathPatterns("/**")
        .excludePathPatterns("/statics/lib/**", "/login", "/statics/pages/login.html");
  }
}
```



#### 登录功能实现

`LoginController`完成登录的校验，而拦截器`LoginInterceptor`负责对是否已登录进行校验。

##### LoginController

登录成功返回true，否则返回false，页面跳转等任务交由前端完成。

登录成功往Session放入一个对象，在后面拦截器中，则以此对象有无判断是否已登录。

```java
@RestController
@RequestMapping("/login")
public class LoginController {

  @GetMapping
  public Boolean login(
      String username,
      String password,
      HttpSession session)
  {
    if (username != null && password != null &&
        username.equals("admin") && password.equals("admin"))
    {
      session.setAttribute(LoginInterceptor.HAS_LOGIN, new Object());
      return true;
    }
    else {
      return false;
    }
  }
}
```



##### LoginInterceptor

使用拦截器中的`preHandle()`方法进行登录验证，逻辑很简单，向Session对象中取某特定对象，取到就是已登录，否则未登录，进行登录页面的跳转。

```java
public class LoginInterceptor implements HandlerInterceptor {

  public static final String HAS_LOGIN = "hasLogin";

  public static final String LOGIN_PAGE = "/statics/pages/login.html";

  @Override
  public boolean preHandle(
    HttpServletRequest request, 
    HttpServletResponse response, 
    Object handler) throws Exception 
  {
    
    HttpSession session = request.getSession();
    Object hasLogin = session.getAttribute(HAS_LOGIN);
    if (hasLogin == null) {
      response.sendRedirect(LOGIN_PAGE);
      return false;
    }else {
      return true;
    }
    
  }
}
```

