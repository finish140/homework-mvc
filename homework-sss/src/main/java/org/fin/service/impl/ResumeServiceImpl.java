package org.fin.service.impl;

import org.fin.pojo.Resume;
import org.fin.repository.ResumeRepository;
import org.fin.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResumeServiceImpl implements ResumeService {

  private ResumeRepository resumeRepository;

  @Autowired
  public ResumeServiceImpl(ResumeRepository resumeRepository) {
    this.resumeRepository = resumeRepository;
  }

  @Override
  public List<Resume> findAll() {
    return resumeRepository.findAll();
  }

  @Override
  public void updateById(Resume resume) {
    resumeRepository.save(resume);
  }

  @Override
  public Resume saveResume(Resume resume) {
    if (resume.getId() != null) resume.setId(null);
    return resumeRepository.saveAndFlush(resume);
  }

  @Override
  public void deleteResumeById(Long id) {
    resumeRepository.deleteById(id);
  }
}
