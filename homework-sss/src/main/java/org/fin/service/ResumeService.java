package org.fin.service;

import org.fin.pojo.Resume;

import java.util.List;

public interface ResumeService {

  List<Resume> findAll();

  void updateById(Resume resume);

  Resume saveResume(Resume resume);

  void deleteResumeById(Long id);
}
