package org.fin.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.lang.annotation.*;


@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "org.fin.repository")
@ComponentScan("org.fin.service")
@PropertySource("classpath:jdbc.properties")
@Configuration
public class SpringConfiguration {

  @Value("${jdbc.driverClass}")
  private String driverClass;
  @Value("${jdbc.url}")
  private String url;
  @Value("${jdbc.username}")
  private String username;
  @Value("${jdbc.password}")
  private String password;

  @Bean(initMethod = "init", destroyMethod = "close")
  public DruidDataSource dataSource() {
    DruidDataSource dataSource = new DruidDataSource();
    dataSource.setDriverClassName(driverClass);
    dataSource.setUrl(url);
    dataSource.setUsername(username);
    dataSource.setPassword(password);
    return dataSource;
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory(
      DataSource dataSource,
      JpaVendorAdapter jpaVendorAdapter)
  {
    LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
    // 设置数据源
    factoryBean.setDataSource(dataSource);
    // 设置实体类的包扫描
    factoryBean.setPackagesToScan("org.fin.pojo");
    // 设置JPA具体实现：Hibernate
    factoryBean.setPersistenceProvider(new HibernatePersistenceProvider());
    // 设置JPA方言：Hibernate
    factoryBean.setJpaDialect(new HibernateJpaDialect());
    // 配置具体Provider，Hibernate框架的具体细节s
    factoryBean.setJpaVendorAdapter(jpaVendorAdapter);
    return factoryBean;
  }

  @Bean
  public JpaVendorAdapter jpaVendorAdapter() {
    HibernateJpaVendorAdapter jva = new HibernateJpaVendorAdapter();
    // 是否自动创建表
    jva.setGenerateDdl(false);
    // 是否打印执行的sql
    jva.setShowSql(true);
    // 配置数据库类型
    jva.setDatabase(Database.MYSQL);
    // 配置数据库方言
    jva.setDatabasePlatform("org.hibernate.dialect.MySQLDialect");
    return jva;
  }

  @Bean
  public TransactionManager transactionManager(DataSource dataSource, EntityManagerFactory entityManagerFactory) {
    JpaTransactionManager txm = new JpaTransactionManager();
    txm.setEntityManagerFactory(entityManagerFactory);
    txm.setDataSource(dataSource);
    return txm;
  }

}
