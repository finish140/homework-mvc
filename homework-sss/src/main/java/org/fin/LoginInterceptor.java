package org.fin;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginInterceptor implements HandlerInterceptor {

  public static final String HAS_LOGIN = "hasLogin";

  public static final String LOGIN_PAGE = "/statics/pages/login.html";

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    HttpSession session = request.getSession();
    Object hasLogin = session.getAttribute(HAS_LOGIN);
    if (hasLogin == null) {
      response.sendRedirect(LOGIN_PAGE);
      return false;
    }else {
      return true;
    }
  }
}
