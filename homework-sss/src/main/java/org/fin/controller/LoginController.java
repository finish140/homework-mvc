package org.fin.controller;

import org.fin.LoginInterceptor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/login")
public class LoginController {

  @GetMapping
  public Boolean login(
      String username,
      String password,
      HttpSession session)
  {
    if (username != null && password != null &&
        username.equals("admin") && password.equals("admin"))
    {
      session.setAttribute(LoginInterceptor.HAS_LOGIN, new Object());
      return true;
    }
    else {
      return false;
    }
  }
}
