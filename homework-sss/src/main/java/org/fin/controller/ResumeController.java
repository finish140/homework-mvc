package org.fin.controller;

import org.fin.pojo.Resume;
import org.fin.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/resume")
public class ResumeController {

  private ResumeService resumeService;

  @Autowired
  public ResumeController(ResumeService resumeService) {
    this.resumeService = resumeService;
  }

  @GetMapping("/list")
  public List<Resume> list() {
    return resumeService.findAll();
  }

  @PutMapping(value = "/{id}", produces = "text/plain;charset=UTF-8")
  public String updateById(Resume resume) {
    resumeService.updateById(resume);
    return "修改成功！";
  }

  @PostMapping(value = "/")
  public Resume saveResume(Resume resume) {
    return resumeService.saveResume(resume);
  }

  @DeleteMapping(value = "/{id}", produces = "text/plain;charset=UTF-8")
  public String deleteResumeById(@PathVariable Long id) {
    resumeService.deleteResumeById(id);
    return "删除成功！";
  }
}
