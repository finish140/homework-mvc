package org.fin.test;

import org.fin.config.SpringConfiguration;
import org.fin.config.SpringMvcConfiguration;
import org.fin.pojo.Resume;
import org.fin.repository.ResumeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
    @ContextConfiguration(classes = SpringConfiguration.class),
    @ContextConfiguration(classes = SpringMvcConfiguration.class)
})
public class SSSTest {

  @Autowired
  private ResumeRepository resumeRepository;

  @Test
  public void testSSS() {
    List<Resume> list = resumeRepository.findAll();
    for (Resume resume : list) {
      System.out.println(resume);
    }
  }
}
