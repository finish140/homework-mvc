package org.fin.mvc.core;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class Handler {

  /** Handler对应的uri */
  private Pattern uri;

  /** Handler方法 */
  private Method method;

  /** Handler所在的Controller对象 */
  private Object controller;

  /** 参数位置映射，用于进行Handler参数的绑定 */
  private Map<String, Integer> paramIndexMapping;

  /** 表示handler可以被哪些用户访问 */
  private Set<String> usernames;

  public Handler(Pattern uri, Method method, Object controller) {
    this.uri = uri;
    this.method = method;
    this.controller = controller;
    this.paramIndexMapping = new HashMap<>(4);
    this.usernames = new HashSet<>(4);
  }

  public Pattern getUri() {
    return uri;
  }

  public void setUri(Pattern uri) {
    this.uri = uri;
  }

  public Method getMethod() {
    return method;
  }

  public void setMethod(Method method) {
    this.method = method;
  }

  public Object getController() {
    return controller;
  }

  public void setController(Object controller) {
    this.controller = controller;
  }

  public Map<String, Integer> getParamIndexMapping() {
    return paramIndexMapping;
  }

  public void setParamIndexMapping(Map<String, Integer> paramIndexMapping) {
    this.paramIndexMapping = paramIndexMapping;
  }

  public Set<String> getUsernames() {
    return usernames;
  }

  public void setUsernames(Set<String> usernames) {
    this.usernames = usernames;
  }

  @Override
  public String toString() {
    return "Handler{" +
        "uri=" + uri +
        ", method=" + method +
        ", controller=" + controller +
        ", paramIndexMapping=" + paramIndexMapping +
        ", usernames=" + usernames +
        '}';
  }
}
