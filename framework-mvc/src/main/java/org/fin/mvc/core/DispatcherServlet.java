package org.fin.mvc.core;

import org.fin.mvc.annotation.*;
import org.fin.mvc.exception.BeanException;
import org.fin.mvc.exception.HandlerException;
import org.fin.mvc.util.StringUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DispatcherServlet extends HttpServlet {

  public static final String CONTEXT_LOCATION = "contextLocation";

  private Properties properties = new Properties();

  private List<Class<?>> componentClasses = new ArrayList<>(16);

  private Map<String, Object> beans = new HashMap<>(64);

  private Map<Class<?>, Set<String>> beanNamesByType = new HashMap<>(64);

  private List<Handler> handlerMapping = new ArrayList<>(16);

  @Override
  public void init(ServletConfig config) {
    String contextLocation = config.getInitParameter(CONTEXT_LOCATION);

    doLoadConfig(contextLocation);

    doScanComponents(properties.getProperty("scanPackage"));

    doInitializeBeans();

    doBeansInjection();

    initHandlerMapping();

    System.out.println("mvc启动完成");
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    doPost(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    Handler handler = getHandler(req);

    if (handler == null) {
      resp.setStatus(404);
      resp.getWriter().write("404 NOT FOUND!");
      return;
    }

    // 验证访问handle的权限
    Set<String> usernames = handler.getUsernames();
    if (usernames.size() > 0) {
      String username = req.getParameter("username");
      boolean hasAuth = false;

      for (String s : usernames) {
        if (s.equals(username)) {
          hasAuth = true;
          break;
        }
      }

      // 没有权限，返回
      if (!hasAuth) {
        resp.setCharacterEncoding("UTF-8");
        resp.setStatus(403);
        resp.setHeader("Content-type", "text/html;charset=UTF-8");
        resp.getWriter().write("用户: " + username + " 没有访问此地址的权限");
        return;
      }
    }

    // 拼装参数
    Object[] args = new Object[handler.getParamIndexMapping().size()];
    handler.getParamIndexMapping().forEach((paramName, i) -> {
      if (paramName.equals(HttpServletRequest.class.getSimpleName())) {
        args[i] = req;
      }
      else if (paramName.equals(HttpServletResponse.class.getSimpleName())) {
        args[i] = resp;
      }
      else {
        args[i] = req.getParameter(paramName);
      }
    });

    // 执行handler
    try {
      Method method = handler.getMethod();
      method.setAccessible(true);
      method.invoke(handler.getController(), args);

      resp.setStatus(200);
      resp.setCharacterEncoding("UTF-8");
      resp.setHeader("Content-type", "text/html;charset=UTF-8");
      resp.getWriter().write("访问成功！");

    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      throw new HandlerException(e);
    }
  }

  /**
   * 根据请求获取handler
   */
  private Handler getHandler(HttpServletRequest req) {
    if (handlerMapping.isEmpty()) return null;

    String requestURI = req.getRequestURI();
    for (Handler handler : handlerMapping) {
      Matcher matcher = handler.getUri().matcher(requestURI);
      if (matcher.matches()) {
        return handler;
      }
    }

    return null;
  }

  /**
   * 建立url和handler的映射关系
   */
  private void initHandlerMapping() {
    if (beans.size() == 0) return;

    for (Object bean : beans.values()) {

      // 找出所有标注类@Controller的bean
      Class<?> beanClass = bean.getClass();
      if (beanClass.isAnnotationPresent(Controller.class)) {


        // 如果在Controller的类上标注了@RequestMapping，读取其中的value作为uri前缀
        String baseUri = "";
        if (beanClass.isAnnotationPresent(RequestMapping.class)) {
          RequestMapping requestMapping = beanClass.getDeclaredAnnotation(RequestMapping.class);
          baseUri += getUrl(requestMapping);
        }

        // 如果在Controller的类上标注了@Security，读取其中的用户名信息
        Set<String> usernames = new HashSet<>(4);
        if (beanClass.isAnnotationPresent(Security.class)) {
          Security security = beanClass.getDeclaredAnnotation(Security.class);
          String[] value = security.value();
          if (value.length > 0) {
            usernames.addAll(Arrays.asList(value));
          }
        }

        // 获取Controller类中，所有标注了@RequestMapping的方法，并封装成handler，注册到#handlerMapping中
        Method[] methods = beanClass.getDeclaredMethods();
        for (Method method : methods) {
          if (method.isAnnotationPresent(RequestMapping.class)) {
            RequestMapping requestMapping = method.getDeclaredAnnotation(RequestMapping.class);
            String uri = baseUri + getUrl(requestMapping);
            Handler handler = new Handler(Pattern.compile(uri), method, bean);

            Parameter[] parameters = method.getParameters();
            for (int i = 0; i < parameters.length; i++) {
              Parameter parameter = parameters[i];

              // 如果参数类型是HttpServletRequest或者HttpServletResponse，则以类名为key
              if (parameter.getType() == HttpServletRequest.class ||
                  parameter.getType() == HttpServletResponse.class)
              {
                handler.getParamIndexMapping().put(parameter.getType().getSimpleName(), i);
              }
              // 否则以参数名为key
              else {
                handler.getParamIndexMapping().put(parameter.getName(), i);
              }
            }

            // 如果handler方法标注了@Security，读取其中的用户名信息，与Controller的@Security中的用户名信息进行合并
            if (method.isAnnotationPresent(Security.class)) {
              Security security = method.getDeclaredAnnotation(Security.class);
              String[] value = security.value();
              if (value.length > 0) {
                // 添加所有方法上的用户权限
                handler.getUsernames().addAll(Arrays.asList(value));
              }
            }

            // 添加所有类上的用户权限
            handler.getUsernames().addAll(usernames);
            // 最后将封装完毕的handler加入到#handlerMapping中
            handlerMapping.add(handler);
          }
        }
      }
    }
  }

  private String getUrl(RequestMapping requestMapping) {
    String value = requestMapping.value().trim();
    if (!value.equals("")) {
      if (!value.startsWith("/")) {
        value = "/" + value;
      }
      return value;
    }else {
      throw new HandlerException("请配置正确的hander的url");
    }
  }

  /**
   * 完成bean中标注了@Autowired属性的注入
   */
  private void doBeansInjection() {
    if (beans.size() == 0) return;

    for (Object bean : beans.values()) {
      Class<?> beanClass = bean.getClass();
      Field[] fields = beanClass.getDeclaredFields();

      for (Field field : fields) {
        if (field.isAnnotationPresent(Autowired.class)) {
          Class<?> fieldType = field.getType();

          Set<String> beanNames = beanNamesByType.get(fieldType);
          if (beanNames != null && beanNames.size() != 0) {
            if (beanNames.size() == 1) {
              String injectBeanName = beanNames.iterator().next();
              Object injectBean = beans.get(injectBeanName);
              try {
                field.setAccessible(true);
                field.set(bean, injectBean);
              } catch (IllegalAccessException e) {
                e.printStackTrace();
              }
            }else {
              throw new BeanException("在 " + beanClass.getName() + " 中，属性"
                  + field.getName() + " 匹配的bean多于一个");
            }
          }

          else {
            throw new BeanException("在 " + beanClass.getName() + " 中，没有与属性"
                + field.getName() + " 匹配的bean");
          }
        }
      }
    }
  }

  /**
   * 初始化bean
   */
  private void doInitializeBeans() {
    if (componentClasses.isEmpty()) return;


    for (Class<?> componentClass : componentClasses) {

      String beanName;
      Object bean;

      // 获取beanName
      if (componentClass.isAnnotationPresent(Controller.class)) {
        Controller ann = componentClass.getDeclaredAnnotation(Controller.class);
        String value = ann.value();
        beanName = getBeanName(componentClass, value);
      } else if (componentClass.isAnnotationPresent(Service.class)) {
        Service ann = componentClass.getDeclaredAnnotation(Service.class);
        String value = ann.value();
        beanName = getBeanName(componentClass, value);
      } else {
        throw new BeanException("找不到对应的注解");
      }

      // 实例化bean，并注册bean
      try {
        Constructor<?> constructor = componentClass.getConstructor();
        constructor.setAccessible(true);
        bean = constructor.newInstance();
        registerBean(beanName, bean);

      } catch (NoSuchMethodException e) {
        throw new BeanException("bean: " + componentClass.getName() + " 没有无参构造");
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InstantiationException e) {
        throw new BeanException("bean: " + componentClass.getName() + " 是抽象类");
      } catch (InvocationTargetException e) {
        throw new BeanException("bean: " + componentClass.getName() + " 的无参构造抛出了一个异常", e);
      }
    }
  }

  /**
   * 注册一个bean，将bean放入单例池中，将bean对应的类型（包含父类以及接口）放入beanNamesByType的map中
   */
  private void registerBean(String beanName, Object bean) {
    List<Class<?>> allClasses = new ArrayList<>(16);

    // 获取一个bean的所有父类以及接口的class对象
    Class<?> cls = bean.getClass();
    while (cls != Object.class) {
      Class<?>[] interfaces = cls.getInterfaces();
      allClasses.addAll(Arrays.asList(interfaces));
      allClasses.add(cls);

      // 接着找父类
      cls = cls.getSuperclass();
    }

    // 将bean对应的所有类型，与beanName对应，放入#beanNamesByType中
    allClasses.forEach(aClass -> {
      if (beanNamesByType.containsKey(aClass)) {
        Set<String> beanNames = beanNamesByType.get(aClass);
        beanNames.add(beanName);
      }else {
        Set<String> beanNames = new HashSet<>(4);
        beanNames.add(beanName);
        beanNamesByType.put(aClass, beanNames);
      }
    });

    // 将bean与beanName放入单例池中
    beans.put(beanName, bean);
  }

  /**
   * 根据注解的value值，获取指定class的beanName
   */
  private String getBeanName(Class<?> componentClass, String value) {
    if (value.trim().equals("")) {
      return StringUtils.toLowerFirstLetter(componentClass.getSimpleName());
    }else {
      return value;
    }
  }

  /**
   * 扫描指定包以及其子包中，包含@Controller和@Service的类，并放入#componentClasses中
   */
  private void doScanComponents(String scanPackage) {
    String path = Objects.requireNonNull(Thread.currentThread()
        .getContextClassLoader()
        .getResource(""))
        .getPath()
        + scanPackage.replaceAll("\\.", "/");

    File pck = new File(path);
    File[] files = pck.listFiles();
    if (files != null && files.length > 0) {
      for (File file : files) {
        // 如果是路径，则进行递归扫描
        if (file.isDirectory()) {
          doScanComponents(scanPackage + "." + file.getName());
        }

        // 如果是文件并且以.class结尾，则进行类名的拼装，以及加载其class对象，进行存储
        else if (file.getName().endsWith(".class")) {
          String className = scanPackage + "." + file.getName().replaceAll(".class", "");
          try {
            Class<?> clz = Class.forName(className);
            if (clz.isAnnotationPresent(Controller.class) || clz.isAnnotationPresent(Service.class)) {
              componentClasses.add(clz);
            }

          } catch (ClassNotFoundException e) {
            e.printStackTrace();
          }
        }
      }
    }

  }

  /**
   * 读取指定位置的配置文件，获取包扫描路径
   */
  private void doLoadConfig(String contextLocation) {
    InputStream in = this.getClass().getClassLoader().getResourceAsStream(contextLocation);

    try {
      assert in != null;
      properties.load(in);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
