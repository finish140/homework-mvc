package org.fin.mvc.util;

public class StringUtils {

  public static String toLowerFirstLetter(String s) {
    return s.substring(0, 1).toLowerCase() + s.substring(1);
  }
}
