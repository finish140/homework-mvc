package org.fin.mvc.exception;

public class MvcException extends RuntimeException {
  public MvcException() {
  }

  public MvcException(String message) {
    super(message);
  }

  public MvcException(String message, Throwable cause) {
    super(message, cause);
  }

  public MvcException(Throwable cause) {
    super(cause);
  }

  public MvcException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
