package org.fin.mvc.example.controller;

import org.fin.mvc.annotation.Controller;
import org.fin.mvc.annotation.RequestMapping;
import org.fin.mvc.annotation.Security;

import javax.servlet.http.HttpServletRequest;

@Security("隔壁老王")
@Controller
@RequestMapping("/demo")
public class DemoController {

  @Security("绿绿")
  @RequestMapping("/xiaohong")
  public void xiaohong(HttpServletRequest req , String s) {
  }

  @Security("大绿子")
  @RequestMapping("/renqi")
  public void renqi(String msg) {}


}
