package org.fin.mvc.annotation;

import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Controller {
  /** Controller的bean id，不配置默认为类名首字母小写 */
  String value() default "";
}
